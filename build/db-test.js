'use strict';

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var sequelize = new _sequelize2.default('health_inditex', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

var Task = sequelize.define('task', {
  title: _sequelize2.default.STRING,
  description: _sequelize2.default.TEXT,
  deadline: _sequelize2.default.DATE
});

sequelize.authenticate().then(function () {
  console.log('Connection has been established successfully.');
  Task.sync({ force: true }).then(function () {
    Task.findAll().then(function (tasks) {
      console.log(tasks);
    });
  });
}).catch(function (err) {
  console.error('Unable to connect to the database:', err);
});