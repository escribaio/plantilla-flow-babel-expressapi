'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _flowTest = require('../../flow-test');

var _flowTest2 = _interopRequireDefault(_flowTest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.get('/list', function (req, res) {
  res.json([{ id: 1, name: 'prueba1' }, { id: 2, name: 'prueba2' }]);
});

router.use('/challenges', router);

exports.default = router;