'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _challenge = require('./challenge');

var _challenge2 = _interopRequireDefault(_challenge);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();
var router = _express2.default.Router();

app.use('/api', _challenge2.default);

app.listen(3000, function () {

  console.log('Api listening on port 3000!');
});