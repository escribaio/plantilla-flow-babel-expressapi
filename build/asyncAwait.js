'use strict';

var main = function () {
  var _ref2 = _asyncToGenerator(regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.t0 = log;
            _context.next = 3;
            return pasaTiempos({ time: 1000, action: 'jugando' });

          case 3:
            _context.t1 = _context.sent;
            (0, _context.t0)(_context.t1);
            _context.t2 = log;
            _context.next = 8;
            return pasaTiempos({ time: 2000, action: 'paseando' });

          case 8:
            _context.t3 = _context.sent;
            (0, _context.t2)(_context.t3);
            _context.t4 = log;
            _context.next = 13;
            return pasaTiempos({ time: 3000, action: 'estudiando' });

          case 13:
            _context.t5 = _context.sent;
            (0, _context.t4)(_context.t5);

          case 15:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function main() {
    return _ref2.apply(this, arguments);
  };
}();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function pasaTiempos(_ref) {
  var time = _ref.time,
      action = _ref.action;

  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      resolve('has pasado ' + time + ' milisegundos ' + action);
    }, time);
  });
}


function log(content) {
  console.log(content);
}

main();