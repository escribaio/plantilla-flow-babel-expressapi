
import express from 'express';
import challengeApi from './challenges';

const app = express()
const router = express.Router()


app.use('/api', challengeApi)


app.listen(3000, function () {

  console.log('Api listening on port 3000!')
})
