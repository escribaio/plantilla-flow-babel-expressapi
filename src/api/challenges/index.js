import express from 'express';
import flowExample from '../../flow-test'

const router = express.Router();

router.get('/', (req, res) => {
  res.json([{id: 1, name: 'prueba1'}, {id: 2, name: 'prueba2'}])
})

router.use('/challenges', router)

export default router;
