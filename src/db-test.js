import Sequelize from 'sequelize';

const sequelize = new Sequelize('health_inditex', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

const Task = sequelize.define('task', {
  title: Sequelize.STRING,
  description: Sequelize.TEXT,
  deadline: Sequelize.DATE
})

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
    Task.sync({force: true}).then(() => {
      Task.findAll().then(tasks => {
        console.log(tasks)
      })
    })

  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });
