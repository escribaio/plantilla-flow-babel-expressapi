
// @flow
function pasaTiempos ({time, action}) : Promise {
  return new Promise((resolve, reject) => {
    setTimeout(function () {
      resolve(`has pasado ${time} milisegundos ${action}`)
    }, time);
  })
}

// @flow
function log(content) {
  console.log(content);
}

async function main() {
  log(await pasaTiempos({time: 1000, action: 'jugando'}));
  log(await pasaTiempos({time: 2000, action: 'paseando'}));
  log(await pasaTiempos({time: 3000, action: 'estudiando'}));
}

main()
